// Include Libraries
#include "Arduino.h"
#include "Buzzer.h"
#include "dht11.h"
#include "LiquidCrystal.h"
#include "Adafruit_NeoPixel.h"

// Pin Definitions

#define DHT_PIN_DATA    3
#define LCD_PIN_RS    9
#define LCD_PIN_E    8
#define LCD_PIN_DB4    4
#define LCD_PIN_DB5    5
#define LCD_PIN_DB6    6
#define LCD_PIN_DB7    7
#define PIN_LED_R    12
#define PIN_LED_G    11
#define PIN_LED_B    10
#define LedRGB_NUMOFLEDS 1
int sensorPin = A0;
int sensorValue = 0;
const int buzPin  = 2;

// object initialization

dht11 DHT11;
LiquidCrystal lcd(LCD_PIN_RS,LCD_PIN_E,LCD_PIN_DB4,LCD_PIN_DB5,LCD_PIN_DB6,LCD_PIN_DB7);

void setup() 
{
    // MONITOR
    Serial.begin(9600);
    // LCD
    lcd.begin(16, 2);
    // LED RGB
    pinMode(PIN_LED_R, OUTPUT);
    pinMode(PIN_LED_G, OUTPUT);
    pinMode(PIN_LED_B, OUTPUT);
}

//<-----------------------------FUNCTIONS------------------------------>

void LedGreenOn()
{
  digitalWrite(PIN_LED_R, LOW);
  digitalWrite(PIN_LED_G, HIGH);
  digitalWrite(PIN_LED_B, LOW);    
}

void LedRedOn()
{
  digitalWrite(PIN_LED_R, HIGH);
  digitalWrite(PIN_LED_G, LOW);
  digitalWrite(PIN_LED_B, LOW);    
}

void playPassed() { /* function playPassed */
    int melody[] = { NOTE_C5, NOTE_C6, NOTE_D5, NOTE_A6 };
    int duration = 200;
    for (int thisNote = 0; thisNote < 4; thisNote++) {
      tone(buzPin, melody[thisNote], duration);
      delay(200);
  }
}
//<------------------------------------------------------------------->

void loop() 
{
    DHT11.read(DHT_PIN_DATA);
    lcd.setCursor(0, 0);
    lcd.print((float)DHT11.temperature, 2);
    lcd.print(" DEGRES C");
    lcd.setCursor(0, 1);
    lcd.print((float)DHT11.humidity, 2); 
    lcd.print(" % HUMIDITE");

    sensorValue = analogRead(sensorPin);
    Serial.println(sensorValue);
    if (sensorValue < 100){
      Serial.println("Fire Detected");
      LedRedOn();
      playPassed();
    } else {
      LedGreenOn(); 
    }
}
